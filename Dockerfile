FROM ubuntu:latest

RUN apt-get update && apt-get install -y cron rsyslog sendmail supervisor
RUN chmod a-x /etc/init.d/sendmail

VOLUME ["/etc-template"]
ADD init-cron /init-cron
ADD supervisord.conf /supervisord.conf

CMD ./init-cron && supervisord -n -c supervisord.conf
