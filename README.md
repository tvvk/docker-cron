# Cron

This container runs a cron daemon and sendmail to send mails from the cron daemon

# Configuration

A directory `/etc-template` has to be mounted into the container. The content of this directory will be picked up and copied into
the /etc directory. Place a `crontab` file (or something like `cron.daily/something` and this becomes the cron configuration of 
the container.

If a script has been changed or added, run `docker exec -it <containername> /init-cron` to re-initialize the crontab.


